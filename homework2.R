library("ggplot2")
library("reshape2")

data.P = function(){
  discount=0.95
  pMove = 0.75
  pDoubleMove = 0.15
  pNotMove = 0.1
  
  M1 = matrix(0,6*7,6*7)
  M2 = matrix(0,6*7,6*7)
  M3 = matrix(0,6*7,6*7)
  M4 = matrix(0,6*7,6*7)
  M5 = matrix(0,6*7,6*7)
  
  M = matrix(c(0,0,0,1,0,0,0,1,1,1,1,1,1,1,1,0,0,1,0,0,1,1,0,0,0,0,0,1,1,0,0,1,0,0,1,1,1,1,1,1,1,1),6,7,byrow=TRUE)

  cols = ncol(M)
  rows = nrow(M)

  s = function(M, r1, c1, r2, c2, ncol, value){
    M[(r1-1)*ncol+c1, (r2-1)*ncol+c2] = value
    M
  }

  for(r in 1:nrow(M)){
    for(c in 1:ncol(M)){
      #UP
    if(r>1 && M[r-1,c]==1){
      M1 = s(M1, r, c, r, c, cols, pNotMove)
      if(r>2 && M[r-2,c]==1){
        M1 = s(M1, r, c, r-1, c, cols, pMove)
        M1 = s(M1, r, c, r-2, c, cols, pDoubleMove)
      } else M1 = s(M1, r, c, r-1, c, cols, pMove+pDoubleMove)
    } else M1 = s(M1, r, c, r, c, cols, pNotMove+pMove+pDoubleMove)
    
    #DOWN
    if(r<rows && M[r+1,c]==1){
      M2 = s(M2, r, c, r, c, cols, pNotMove)
      if(r<rows-1 && M[r+2,c]==1){
        M2 = s(M2, r, c, r+1, c, cols, pMove)
        M2 = s(M2, r, c, r+2, c, cols, pDoubleMove)
      } else M2 = s(M2, r, c, r+1, c, cols, pMove+pDoubleMove)
    } else M2 = s(M2, r, c, r, c, cols, pNotMove+pMove+pDoubleMove)
    
    #LEFT
    if(c>1 && M[r,c-1]==1){
      M3 = s(M3, r, c, r, c, cols, pNotMove)
      if(c>2 && M[r,c-2]==1){
        M3 = s(M3, r, c, r, c-1, cols, pMove)
        M3 = s(M3, r, c, r, c-2, cols, pDoubleMove)
      } else M3 = s(M3, r, c, r, c-1, cols, pMove+pDoubleMove)
    } else M3 = s(M3, r, c, r, c, cols, pNotMove+pMove+pDoubleMove)
                  
    #RIGHT
    if(c<cols && M[r,c+1]==1){
      M4 = s(M4, r, c, r, c, cols, pNotMove)
      if(c<cols-1 && M[r,c+2]==1){
        M4 = s(M4, r, c, r, c+1, cols, pMove)
        M4 = s(M4, r, c, r, c+2, cols, pDoubleMove)
      } else M4 = s(M4, r, c, r, c+1, cols, pMove+pDoubleMove)
    } else M4 = s(M4, r, c, r, c, cols, pNotMove+pMove+pDoubleMove)
    
    M5 = s(M5, r, c, r, c, cols, 1)
    }
  }
  
  for (point in c(1,2,3,5,6,7,16,17,19,20,23,24,25,26,27,30,31,33,34)){
    M1[point,]=0
    M2[point,]=0
    M3[point,]=0
    M4[point,]=0
    M5[point,]=0
    
    M1[,point]=0
    M2[,point]=0
    M3[,point]=0
    M4[,point]=0
    M5[,point]=0
  }
  
  
  P = array(0, c(6*7,6*7,5))
  P[,,1]=M1
  P[,,2]=M2
  P[,,3]=M3
  P[,,4]=M4
  P[,,5]=M5
  
  P
}

data.R = function(){
  R = array(0, c(6*7,6*7,5))
  #R[,,]=-1
  R[,18,]=-100
  R[,32,]=100
  R[32,,]=-100
  R[32,32,]=0
  
  R
}

mdp.bellman_operator = function(P, PR, discount, Vprev){
  A = dim(P)[3]
  S = dim(PR)[1]
  
  Q = matrix(0, dim(PR)[1], dim(PR)[2])
  for (a in 1:A) {
    Q[,a] = PR[,a] + discount*P[,,a] %*% Vprev
  }
  
  return(list("V"=apply(Q, 1, max), "policy"=apply(Q, 1, which.max), "Q" = Q ))
}
mdp.value_iteration = function(P, R, discount, max_iter=5000){
  S = dim(P)[1]
  A = dim(P)[3]
  
  V0 = numeric(S)
  epsilon = 0.01
  
  # computation of threshold of variation for V for an epsilon-optimal policy
  if (discount != 1) {
    thresh = epsilon * (1-discount)/discount
  } else {
    thresh = epsilon
  }
  
  PR = array(NA, c(S,A))
  for (a in 1:A) {
    PR[,a] = rowSums(P[,,a]*R[,,a])
  }
  
  iter = 0
  V = V0
  is_done = F
  Q = NULL
  
  while(!is_done) {
    iter = iter + 1
    Vprev = V
    
    bellman = mdp.bellman_operator(P,PR,discount,V)
    V = bellman[[1]]
    policy = bellman[[2]]
    Q = bellman[[3]]
    
    Vdiference = V - Vprev
    variation = max(Vdiference) - min(Vdiference)
    
    if (variation < thresh) {
      is_done = T
      print('MDP: iterations stopped, epsilon-optimal policy found')
    } else if (iter == max_iter) {
      is_done = T
      print('MDP: iterations stopped by maximum number of iteration condition')
    }
  }
  
  return(list("V"=V, "policy"=policy, "iter"=iter, "epsilon" = epsilon, "discount" = discount, "Q" = Q))
}

euclidean_norm = function(x) sqrt(sum(x^2))

qlearning = function(Q, S, learning_rate, discount,
                     function_action, function_take_action, function_select_S,
                     num_iterations=50000){
  Qlist = list()
  Alist = list()
  Slist = list()
  for(i in 1:num_iterations){
    A = function_action(Q, S)
    observation = function_take_action(S, A)
    Q[S,A] = Q[S,A] + learning_rate * (observation$R + discount * max(Q[observation$S,]) - Q[S,A])
    Qlist[[i]] = Q
    Slist[[i]] = S
    Alist[[i]] = A
    S = function_select_S(observation$S)
    
  }
  list(Q=Q,Qlist=Qlist,Slist=Slist, Alist=Alist)
}


sarsa = function(Q, S, learning_rate, discount, final_state,
                     function_action, function_take_action, function_select_S,
                     num_iterations=50000){
  Qlist = list()
  initial_state = S
  A = function_action(Q, S)
  
  for(i in 1:num_iterations){
    observation = function_take_action(S, A)
    A2 = function_action(Q, observation$S)
    Q[S,A] = Q[S,A] + learning_rate * (observation$R + discount * Q[observation$S,A2] - Q[S,A])
    Qlist[[i]] = Q
    
    S = function_select_S(observation$S)
    if(S==observation$S){
      A = A2
    }
    else{
      A = function_action(Q, S)
    }
  }
  list(Q=Q,Qlist=Qlist)
}

qlearning.epsilon_greedy = function(Q, S, epsilon){
  random = runif(1)
  if(random>epsilon){
    maxs = which(Q[S,]==max(Q[S,]))
    #maxs = which.max(Q[S,])
    if(length(maxs)==1) return(maxs)
    return(sample(maxs,1)) #select randomly among the maximums
  }
  else{
    sample(1:dim(Q)[2],1)
  }
}

qlearning.take_action = function(S, A, P, R){
  newS = sample(1:dim(P)[1], 1, prob=P[S,,A]) #Verificar
  newR = R[S,newS,A]
  list(S=newS, R=newR)
}

qlearning.select_S = function(S, S_terminal, space){
  if(S!=S_terminal) S
  else sample(space, 1)
}

qlearning.run = function(){
  P = data.P()
  R = data.R()
  space = c(4,8:14,15,18,21,22,28,29,35,36:42)
  S = 4
  Q_initial = matrix(0, 42, 5)
  num_iterations = 5000
  qlearning(Q_initial, 
            S,
            .05,
            .95,
            function(Q,S){qlearning.epsilon_greedy(Q,S,.2)},
            function(S,A){qlearning.take_action(S,A,P,R)},
            function(S){qlearning.select_S(S,32,space)},
            num_iterations)
}

sarsa.run = function(){
  P = data.P()
  R = data.R()
  space = c(4,8:14,15,18,21,22,28,29,35,36:42)
  S = 4
  Q_initial = matrix(0, 42, 5)
  num_iterations = 5000
  
  sarsa(Q_initial, 
            S,
            .05,
            .95,
            32,
            function(Q,S){qlearning.epsilon_greedy(Q,S,.2)},
            function(S,A){qlearning.take_action(S,A,P,R)},
            function(S){qlearning.select_S(S,32,space)},
            num_iterations)
}

error = function(Qlist,Qbest){
  A = length(Qlist)
  error = vector("numeric", A)
  for(i in 1:A){
    error[i] = euclidean_norm(Qlist[[i]]-Qbest)
  }
  error
}

do6 = function(){
  P = data.P()
  R = data.R()
  data.sarsa = sarsa.run()
  data.qlearning = qlearning.run()
  mdp = mdp.value_iteration(P, R, .95)
  Qbest = mdp$Q
  
  error.sarsa = error(data.sarsa$Qlist,Qbest)
  error.qlearning = error(data.qlearning$Qlist,Qbest)
  
  df = data.frame(sarsa = error.sarsa, qlearning = error.qlearning)
  df$id = as.numeric(rownames(df))
  
  df.melt = melt(df, id="id")
  names(df.melt)=c("id","algorithm","value")
  ggplot(data=df.melt, aes(x=id, y=value, colour=algorithm))+geom_line()+xlab("")+ylab("error")
}

ucb = function(sequence, A_set = 5){
  i = 1
  t_i = array(0, A_set)
  reward = array(0, A_set)
  regret = array(0, length(sequence))
  cumulative_reward = array(0, A_set)
  arm_q = array(0, c(length(sequence),A_set))
  
  for(seq in sequence){
    if(i<=A_set) arm = i
    else arm = which.max(reward / t_i + sqrt(2 * log(i) / t_i))
    
    t_i[arm] = t_i[arm] + 1
    if(arm==seq){
      reward[arm] = reward[arm] + 1
    }
    arm_q[i,]=reward / t_i + sqrt(2 * log(i) / t_i)
    
    cumulative_reward[seq] = cumulative_reward[seq]+1
    
    #regret[i] = max(cumulative_reward)-cumulative_reward[arm]
    regret[i] = max(cumulative_reward)-sum(reward)
    i = i+1
    
  }
  list(regret=regret, reward=reward, t_i=t_i, cumulative_reward = cumulative_reward, arm_q=arm_q)
}

do7 = function(){
  data = read.csv("seqs.csv",header = F, sep = ",")
  data = t(data)
  
  df = data.frame(data1 = ucb(data[,1])$regret, data2=ucb(data[,2])$regret)
  ucb(data[,2])
  
  df$x = as.numeric(rownames(df))
  
  df.melt = melt(df, id="x")
  names(df.melt)=c("x","data","value")
  ggplot(data=df, aes(x=x, colour="data"))+geom_line(aes(y=data1, colour="data1"))+geom_line(aes(y=data2, colour="data2"))+xlab("")+ylab("regret")
}

exp3 = function(sequence, A_set = 5, exploration=.2){
  i = 1
  t_i = array(0, A_set)
  reward = array(0, A_set)
  regret = array(0, length(sequence))
  cumulative_reward = array(0, A_set)
  arm_q = array(0, c(length(sequence),A_set))
  w = array(1, A_set)
  
  for(seq in sequence){
    p = (1-exploration)*w/sum(w) + exploration/A_set
    arm = sample(1:A_set, 1, prob=p)
    
    #if(i<=A_set) arm = i
    #else arm = which.max(reward / t_i + sqrt(2 * log(i) / t_i))
    
    t_i[arm] = t_i[arm] + 1
    r = 0
    if(arm==seq){ r = 1 }
    
    reward[arm] = reward[arm] + r
    w = w * exp(exploration/A_set * r / p[arm])
    
    arm_q[i,]=reward / t_i + sqrt(2 * log(i) / t_i)
    cumulative_reward[seq] = cumulative_reward[seq]+1
    
    
    
    #regret[i] = max(cumulative_reward)-cumulative_reward[arm]
    regret[i] = max(cumulative_reward)-sum(reward)
    i = i+1
    
  }
  list(regret=regret, reward=reward, t_i=t_i, cumulative_reward = cumulative_reward, arm_q=arm_q)
}

do8 = function(){
  data = read.csv("seqs.csv",header = F, sep = ",")
  data = t(data)
  
  df = data.frame(data1 = exp3(data[,1])$regret, data2=exp3(data[,2])$regret)
  
  df$x = as.numeric(rownames(df))
  
  df.melt = melt(df, id="x")
  names(df.melt)=c("x","data","value")
  ggplot(data=df, aes(x=x, colour="data"))+geom_line(aes(y=data1, colour="data1"))+geom_line(aes(y=data2, colour="data2"))+xlab("")+ylab("regret")
}